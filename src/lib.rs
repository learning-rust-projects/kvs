#![allow(unused_variables)]
use std::{collections::HashMap, path::PathBuf};
use std::result::Result as StdResult;
use failure::{Error, err_msg};
use structopt::StructOpt;
use serde::{Serialize, Deserialize};
use std::fs::OpenOptions;
use std::io::prelude::*;

pub type Result<T> = StdResult<T, Error>;
const SET_CMD: &str = "set";

#[derive(StructOpt, Debug, Serialize, Deserialize)]
#[structopt(name = "kvs")]
pub enum Cmd {
    Get { key: String },
    Set { key: String, value: String },
    Rm { key: String },
}

#[derive(Default, Serialize, Deserialize)]
struct Transaction {
    command: &'static str,
    key: String,
    value: String,
}

impl Transaction {
    pub fn new(cmd: &'static str, key: String, value: String) -> Self {
        Self {
            command: cmd,
            key: key,
            value: value
        }
    }
}

#[derive(Default)]
pub struct KvStore {
    kv: HashMap<String, String>,
    pub log: Vec<String>,
}

impl KvStore {
    pub fn set(self: &mut KvStore, key: String, value: String) -> Result<()> {
        let transaction = Transaction::new(SET_CMD, key, value);
        let json = serde_json::to_string(&transaction)?;
        self.log.push(json.clone());
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open("log.txt")?;

        writeln!(&mut file, "{}", json)?;

        Ok(())
    }

    pub fn get(self: &KvStore, key: String) -> Result<Option<String>> {
        self.kv.get(&key);
        Err(err_msg("couldn't get"))
    }

    pub fn remove(self: &mut KvStore, key: String) -> Result<()> {
        self.kv.remove(&key);
        Err(err_msg("couldn't remove"))
    }

    pub fn open(path: impl Into<PathBuf>) -> Result<KvStore> {
        Err(err_msg("couldn't open"))
    }
}