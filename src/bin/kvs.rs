use kvs::{KvStore, Result, Cmd};
use structopt::StructOpt;

fn main() -> Result<()> {
    let cmd = Cmd::from_args();
    let mut store = KvStore::default();
    match cmd {
        Cmd::Get { key } => match store.get(key.clone())? {
            Some(value) => {
              println!("{:?}", value);
              Ok(())
            }
            None => {
              println!("Key not found");
              Ok(())
            }
        },
        Cmd::Set { key , value } => store.set(key, value),
        Cmd::Rm { key } => store.remove(key.clone()),
    }
}
